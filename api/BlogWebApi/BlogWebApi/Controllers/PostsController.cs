﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using BlogWebApi.Models;
using System.Web.Http.Cors;

namespace BlogWebApi.Controllers
{
	[EnableCors(origins: "*", headers: "*", methods: "*")]
	public class PostsController : ApiController
	{
		Post[] posts = new Post[] 
		{ 
			new Post { id = 1, title = "Post 1", body = "Body 1"}, 
			new Post { id = 2, title = "Post 2", body = "Body 2"}, 
			new Post { id = 3, title = "Post 3", body = "Body 3"}, 
			new Post { id = 4, title = "Post 4", body = "Body 4"}, 
			new Post { id = 5, title = "Post 5", body = "Body 5"}, 
			new Post { id = 6, title = "Post 6", body = "Body 6"}, 
			new Post { id = 7, title = "Post 7", body = "Body 7"}, 
		};

		public IHttpActionResult Get(int pageSize = 0, int pageNumber = 0, string searchText = "")
		{
			var totalCount = posts.Length;
			var filteredPosts = posts;
			if (!string.IsNullOrEmpty(searchText))
			{
				filteredPosts = posts.Where(x => x.title.Contains(searchText)).ToArray();
				totalCount = filteredPosts.Length;
			}
			if (pageNumber > 0)
			{
				var skip = (pageNumber - 1) * pageSize;
				filteredPosts = filteredPosts.Skip(skip).Take(pageSize).ToArray();
			}

			var result = new
				{
					totalCount = totalCount,
					posts = filteredPosts
				};
			return Ok(result);
		}

		public IHttpActionResult GetProduct(int id)
		{
			var product = posts.FirstOrDefault((p) => p.id == id);
			if (product == null)
			{
				return NotFound();
			}
			return Ok(product);
		}
	}
}
