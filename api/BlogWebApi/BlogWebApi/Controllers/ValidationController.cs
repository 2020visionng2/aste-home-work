﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace BlogWebApi.Controllers
{
	[EnableCors(origins: "*", headers: "*", methods: "*")]
	public class ValidationController : ApiController
	{
		public IHttpActionResult Get(string title = "")
		{
			var result = title != "notvalid";
			return Ok(result);
		}
	}
}