import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import { Post } from '../components/posts/models/post.model';
import { Configuration } from '../configuration';
import { PostsDataResponse } from './responses/posts-data-response';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx'

@Injectable()
export class WebApiDataService {
	public http: Http;
    private config : Configuration;
    
	constructor(http: Http, config: Configuration) {
        this.http = http;
        this.config = config;
	}

  getPosts(searchText: string, pageSize: number, pageNumber: number) : Observable<PostsDataResponse> {
      var url = this.config.apiBaseAddress + "posts";
      let params: string[] = [];
      if (pageSize) {
          params.push('pageSize=' + pageSize);
        }
        if (pageNumber) {
			params.push('pageNumber=' + pageNumber);
        }
	    if (searchText) {
			params.push('searchText=' + searchText);
        }
		for (let i = 0; i < params.length; i++) {
			url += i === 0 ? '?' : '&';
			url += params[i];
		}  
      return this.http.get(url).map( (responseData) => {
        return responseData.json();
      })
    }

    validateTitle(title: string) : Observable<boolean> {
        var url = this.config.apiBaseAddress + "validation";
        let params: string[] = [];
        params.push('title=' + title);
        url += params;
        return this.http.get(url).map( (responseData) => {
            return responseData.json();
      })
    }
}