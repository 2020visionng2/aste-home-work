import { Post } from '../../components/posts/models/post.model';

export class PostsDataResponse {
    public totalCount : number;
    public posts: Post[];
}