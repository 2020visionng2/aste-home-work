import { Injectable } from 'angular2/core';

import { Post } from '../components/posts/models/post.model';
import { DataService } from './data-service';

@Injectable()
export class LocalStorageDataService extends DataService {
	private key: string = 'posts';

	public getPosts(): Post[] {
		let posts = localStorage.getItem(this.key);
		return posts ? JSON.parse(posts) : [];
	}

	public savePost(post: Post): void {
		let posts = this.getPosts();
		if (post.id) {
			let existingPost: Post = posts.find(x => x.id === post.id);
			existingPost.update(post);
		} else {
			post.id = posts.length + 1;
			posts.push(post);
		}
		this.savePosts(posts);
	}

	public getPost(id: number): Post {
		let posts = this.getPosts();
		return posts.find(x => x.id === id);
	}

	private savePosts(posts: Post[]): void {
		localStorage.setItem(this.key, JSON.stringify(posts));
	}
}