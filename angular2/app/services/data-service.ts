import { Injectable } from 'angular2/core';

import { Post } from '../components/posts/models/post.model';

@Injectable()
export abstract class DataService {
	public abstract getPosts(): Post[];
	public abstract getPost(id: number): Post;
	public abstract savePost(post: Post): void;
}