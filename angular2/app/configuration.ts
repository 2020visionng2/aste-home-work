import { Injectable } from 'angular2/core';

export class Configuration {
    public apiBaseAddress : string;
    constructor() {
        this.apiBaseAddress = 'http://localhost:7010/api/';
    }
}