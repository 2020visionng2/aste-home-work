import { NgFor } from 'angular2/common';
import { Component, View, Input, OnChanges, EventEmitter, Output} from 'angular2/core';

@Component({
	selector: 'pager'
})

@View({
	templateUrl: './components/common_controls/pager/pager.html'
})

export class PagerComponent implements OnChanges {
    public pages: number[];
    @Input() public totalCount: number;
    @Input() public pageSize: number;
    @Input() public currentPage: number;
    @Output() pageChanged: EventEmitter<number> = new EventEmitter();
    
    constructor(){

    }
    
    public ngOnChanges(changes: {}): any {
		let pagesCount = Math.ceil(this.totalCount / this.pageSize);
        this.pages = [];
        for (var i = 1; i <= pagesCount; i++) {
            this.pages.push(i);
        }
	}
    
    public onPageChanging(page): void {
		if (page === this.currentPage) {
			return;
		}

		this.currentPage = page;
		this.pageChanged.emit(page);
	}
}