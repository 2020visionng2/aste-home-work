import {
	Component,
	View,
	OnInit
} from 'angular2/core';
import {
	RouteParams
} from 'angular2/router';

import { PostsService } from './../posts.service';
import { Post } from './../models/post.model';

@Component({
	selector: 'post-edit'
})
@View({
	templateUrl: './components/posts/item/post-item.html'
})
export class PostItemComponent implements OnInit {
	private postsService: PostsService;
	private postId: number;
	private post: Post;

	constructor(postsService: PostsService, routeParams: RouteParams) {
		this.postsService = postsService;
		this.postId = parseInt(routeParams.get('id'), 10);
	}

	ngOnInit(): any {
		this.post = this.postsService.getPost(this.postId);
	}
}