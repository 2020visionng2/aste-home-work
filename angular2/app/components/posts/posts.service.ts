import {Injectable} from 'angular2/core';
import {Post} from './models/post.model';
import {DataService} from '../../services/data-service';
import {WebApiDataService} from '../../services/web-api-data-service';
import { PostsDataResponse } from '../../services/responses/posts-data-response';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PostsService {
	private postss: Post[];
	private dataService: DataService;
	private webApiDataService : WebApiDataService;

	constructor(dataService: DataService, webApiDataService : WebApiDataService) {
		this.dataService = dataService;
		this.webApiDataService = webApiDataService;
	}
	
	public getPosts(searchText: string, pageSize: number, pageNumber: number) : Observable<PostsDataResponse> {
		return this.webApiDataService.getPosts(searchText, pageSize, pageNumber);
	}

	getPost(postId: number): Post {
		return this.dataService.getPost(postId);
	}
    
    validateTitle(title: string) : Observable<boolean> {
        return this.webApiDataService.validateTitle(title);
    }
}