import {
	Component,
	View
} from 'angular2/core';

import {
	FORM_DIRECTIVES,
	FormBuilder,
	ControlGroup,
	Validators
} from 'angular2/common';

import { Router } from 'angular2/router';

import { PostsService } from './../posts.service';
import { Post } from './../models/post.model';
import { Observable } from 'rxjs/Observable';
import * as Rx from "rxjs/Rx";

@Component({
	selector: 'post-edit'
})
@View({
	templateUrl: './components/posts/edit/post-edit.html',
	directives: [FORM_DIRECTIVES]
})
export class PostEditComponent {
	private postsService: PostsService;
	private router: Router;
	public form: ControlGroup;

	constructor(postsService: PostsService, fb: FormBuilder, router: Router) {
		this.postsService = postsService;
		this.router = router;
		this.buildForm(fb);
	}

    public containsMagicWord(c: Control) {
        this.postsService.validateTitle(c.value).subscribe( x => {
            if (x == true) {
                return true;
            }
            else {
                return false;
            }
        })
    }
    
    static wrongTitle(control: Control): Promise<ValidationResult> {
        let q = new Promise((resolve, reject) => {
            setTimeout(() => {
                if (control.value === 'notvalid') {
                    resolve({'wrongTitle': true});
                }
                else {
                     resolve(null);
                    }
            }, 1000)
        });
        return q;
    }
 
	public submit(): void {
		//this.postsService.save(<Post>this.form.value);
		this.goToPosts();
	}

	public cancel(): void {
		this.goToPosts();
	}

	private goToPosts(): void {
		this.router.navigate(['/Posts']);
	}

	private buildForm(fb: FormBuilder): void {
		this.form = fb.group({
			title: ['', Validators.required, PostEditComponent.wrongTitle],
			body: ['', Validators.required]
		});
	}
}