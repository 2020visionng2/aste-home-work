import { Component, View } from 'angular2/core';
import { RouterLink } from 'angular2/router';
import { Post } from '../models/post.model';

@Component({
	selector: 'post-brief',
	inputs: ['post']
})
@View({
	templateUrl: './components/posts/list/post-brief.html',
	directives: [RouterLink]
})
export class PostsBriefComponent {
	public post: Post;
}