import { Component, View, OnInit } from 'angular2/core';
import { NgFor } from 'angular2/common';

import { PostsService } from './../posts.service';
import { Post } from './../models/post.model';
import { PostsBriefComponent } from './post-brief.component';
import { PagerComponent } from '../../common_controls/pager/pager.component';
import { Observable } from 'rxjs/Observable';
import * as Rx from "rxjs/Rx";

@Component({
	selector: 'posts-list'
})
@View({
	templateUrl: './components/posts/list/posts-list.html',
	directives: [PostsBriefComponent, PagerComponent]
})
export class PostsListComponent implements OnInit {

	public currentPosts: Post[];
	
    private postsService: PostsService;
    private searchParam : Rx.Subject<string>;
    private currentSearchString: string;
	private currentPage: number;
	private pageSize: number;
    private totalCount: number;

	constructor(postsService: PostsService) {
		this.postsService = postsService;
        this.currentPage = 1;
        this.totalCount = 1;
        this.pageSize = 5;
        this.searchParam = new Rx.BehaviorSubject<string>("");
        this.searchParam.debounceTime(500)
        .distinctUntilChanged()
        .subscribe(x => {
            this.currentSearchString = x.toString();
            this.loadPosts(this.currentSearchString);
        });
    }

	ngOnInit(): any {
		this.loadPosts("");
	}

    public searchTextEntered(searchString: string) {
        this.searchParam.next(searchString);
    }
    public changePage(page: number): void {
		this.currentPage = page;
		this.loadPosts(this.currentSearchString);
	}
    
	private loadPosts(searchText: string): void {
		this.currentPosts = new Array<Post>();
		this.postsService.getPosts(searchText, this.pageSize, this.currentPage).subscribe(
			(responseData) => {
                this.totalCount = responseData.totalCount;
                this.currentPosts = responseData.posts;
            });
	};
	
}