export class Post {
	public title: string;
	public body: string;
	public id: number;

	constructor(id: number, title: string, body: string) {
		this.id = id;
		this.title = title;
		this.body = body;
	}

	public update(post: Post): void {
		this.title = post.title;
		this.body = post.body;
	}
}