import {Component, ViewEncapsulation} from 'angular2/core';
import {
	RouteConfig,
	ROUTER_DIRECTIVES
} from 'angular2/router';

import {PostsListComponent} from '../posts/list/posts-list.component';
import {PostEditComponent} from '../posts/edit/post-edit.component';
import {PostItemComponent} from '../posts/item/post-item.component';
import {PostsService} from './../posts/posts.service';
import {NavigationComponent} from './navigation.component';

@Component({
	selector: 'app',
	providers: [PostsService],
	templateUrl: './components/app/app.html',
	styleUrls: ['./components/app/app.css'],
	encapsulation: ViewEncapsulation.None,
	directives: [ROUTER_DIRECTIVES, NavigationComponent]
})
@RouteConfig([
	{path: '/', component: PostsListComponent, as: 'Posts'},
	{path: '/add-post', component: PostEditComponent, as: 'AddPost'},
	{path: '/posts/:id', component: PostItemComponent, as: 'Post'},
])
export class AppCmp {
}
