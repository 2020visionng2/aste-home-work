import { View, Component } from 'angular2/core';
import { ROUTER_DIRECTIVES } from 'angular2/router';

@Component({
	selector: 'navigation'
})
@View({
	templateUrl: './components/app/navigation.html',
	directives: [ROUTER_DIRECTIVES]
})
export class NavigationComponent {
}