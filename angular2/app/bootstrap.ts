import {bootstrap} from "angular2/platform/browser"
import {provide} from 'angular2/core';
import {ROUTER_PROVIDERS, APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';

import {AppCmp} from './components/app/app';
import {DataService} from './services/data-service';
import {LocalStorageDataService} from './services/local-storage-data-service';
import {WebApiDataService} from './services/web-api-data-service';
import {Configuration} from './configuration';

bootstrap(AppCmp, [
	provide(APP_BASE_HREF, {useValue: '<%= APP_BASE + APP_DEST %>'}),
	ROUTER_PROVIDERS,
	HTTP_PROVIDERS,
	provide(WebApiDataService, {useClass: WebApiDataService}),
    provide(Configuration, {useClass: Configuration}),
	provide(LocationStrategy, {useClass: HashLocationStrategy}),
	provide(DataService, {useClass: LocalStorageDataService})
]);
